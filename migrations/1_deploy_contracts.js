var Aqua = artifacts.require("Aqua");
var fs = require('fs');

var saveABI = (contract, filename) => {
    abiJSON = JSON.stringify(contract.abi, null, 2); // beautiful indented output.
    fs.writeFile(filename, abiJSON, function(err) {
        if (err) { console.log(err); }
    });
}

module.exports = function(deployer) {   // deployment steps  
  saveABI(Aqua, "./build/AquaABI.json")
  deployer.deploy(Aqua)
};