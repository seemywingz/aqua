pragma solidity 0.4.23;

import "./ERC20/SafeERC20.sol";
import "./Ownable.sol";
import "./ERC20/StandardToken.sol";

contract Aqua is StandardToken, Ownable {
  using SafeERC20 for ERC20;
  using SafeMath for uint256;

  string public name = "Aqua";
  string public symbol = "AQA";
  uint8 public decimals = 18;
  
  uint256 public weiRaised;
  uint256 public rate = 1000;
  bool public mintingFinished = false;

  event Mint(address indexed mintedBy, uint256 amount);
  event MintFinished();

  modifier canMint() {
    require(!mintingFinished);
    _;
  }

  function () public payable {
    mint();
  }

  /**
   * @dev Function to mint tokens based off of rate
   * @return A boolean that indicates if the operation was successful.
   */
  function mint() canMint payable public returns (bool) {
    uint256 weiAmount = msg.value;
    require(weiAmount >= 500000000000000000); // ensure min 0.5 ETH deposit
    weiRaised = weiRaised.add(weiAmount);
    owner.transfer(weiAmount);

    uint256 tokenAmmount = weiAmount.mul(rate);
    totalSupply_ = totalSupply_.add(tokenAmmount);
    balances[msg.sender] = balances[msg.sender].add(tokenAmmount);

    emit Mint(msg.sender, tokenAmmount);
    emit Transfer(address(0), msg.sender, tokenAmmount);

    return true;
  }

  /**
   * @dev Function to stop minting new tokens.
   * @return True if the operation was successful.
   */
  function finishMinting() onlyOwner canMint public returns (bool) {
    mintingFinished = true;
    emit MintFinished();
    return true;
  }
}

