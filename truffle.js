module.exports = {
  solc: {
    optimizer: {
      enabled: true,
      runs: 200
    }
  },
  networks: {
    development: {
      port: 8545,
      host: "127.0.0.1",
      network_id: "1985",
      from: "0x7bCbd43F0Ef48b00fB4b1ACf5A96845Aa8bcB178"
    }
  }
}
